###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = pam
version    = 1.5.3
release    = 1
thisapp    = Linux-PAM-%{version}

groups     = System/Base
url        = https://github.com/linux-pam/linux-pam/
license    = BSD and GPLv2+ and BSD with advertising
summary    = An extensible library which provides authentication for applications.

description
	PAM (Pluggable Authentication Modules) is a system security tool that
	allows system administrators to set authentication policy without
	having to recompile programs that handle authentication.
end

source_dl  = https://github.com/linux-pam/linux-pam/releases/download/v%{version}/
sources    = %{thisapp}.tar.xz

build
	requires
		bison
		cracklib-devel
		flex
		libxcrypt-devel
	end

	export LD_LIBRARY_PATH = %{DIR_APP}/libpam/.libs

	configure_options += \
		--includedir=%{includedir}/security \
		--docdir=/usr/share/doc/Linux-PAM-%{version} \
		--enable-read-both-confs \
		--disable-rpath

	test
		# Temporary copy our pam config files to the sysconfdir
		# the chroot environment. They are required by various tests
		# of the testsuite.
		cp -avf %{DIR_SOURCE}/pam.d %{sysconfdir}

		# Run the testsuite.
		make check
	end

	install_cmds
		#useradd -D -b /home
		#sed -i 's/yes/no/' %{BUILDROOT}/etc/default/useradd
		mkdir -pv %{BUILDROOT}%{sysconfdir}/security
		install -v -m644 %{DIR_SOURCE}/pam_env.conf \
			%{BUILDROOT}%{sysconfdir}/security/pam_env.conf

		# Included in setup package
		rm -f %{BUILDROOT}%{sysconfdir}/environment

		# Install man pages.
		mkdir -pv %{BUILDROOT}%{mandir}/man5
		for file in config-util.5 postlogin.5 system-auth.5; do
			install -v -m 644 %{DIR_SOURCE}/man/${file} \
				%{BUILDROOT}%{mandir}/man5
		done
	end
end

packages
	package %{name}
		requires
			/usr/lib64/security/pam_pwquality.so
		end

		configfiles
			%{sysconfdir}/pam.d
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
