###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = shared-mime-info
version    = 2.2
release    = 1

url        = http://freedesktop.org/Software/shared-mime-info
license    = GPLv2+
summary    = Shared MIME information database

description
	This is the freedesktop.org shared MIME info database.

	Many programs and desktops use the MIME system to represent the types of
	files. Frequently, it is necessary to work out the correct MIME type for
	a file. This is generally done by examining the file's name or contents,
	and looking up the correct MIME type in a database.
end

source_dl  = https://gitlab.freedesktop.org/xdg/shared-mime-info/-/archive/%{version}/

build
	requires
		glib2-devel
		libxml2-devel
		meson >= 0.55.3
	end

	build
		%{meson} \
			-Dupdate-mimedb=true

		%{meson_build}

	end

	install
		%{meson_install}
	end
end

packages
	package %{name}
		script postin
			update-mime-database -n %{datadir}/mime &> /dev/null ||:
		end

		script postup
			update-mime-database -n %{datadir}/mime &> /dev/null ||:
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
