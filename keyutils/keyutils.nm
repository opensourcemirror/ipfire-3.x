###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = keyutils
version    = 1.6.3
release    = 1

groups     = System/Tools
url        = https://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils.git/
license    = GPLv2+ and LGPLv2+
summary    = Linux Key Management Utilities

description
	Utilities to control the kernel key management facility and to provide
	a mechanism by which the kernel call back to user space to get a key
	instantiated.
end

source_dl  = https://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils.git/snapshot/

build
	requires
		kernel-headers
	end

	make_build_targets += \
		NO_ARLIB=1 \
		ETCDIR=%{sysconfdir} \
		LIBDIR=%{libdir} \
		USRLIBDIR=%{libdir} \
		BINDIR=%{bindir} \
		SBINDIR=%{sbindir} \
		MANDIR=%{mandir} \
		INCLUDEDIR=%{includedir} \
		SHAREDIR=%{datadir}/%{name} \
		NO_GLIBC_KEYERR=1 \
		CFLAGS="-Wall %{CFLAGS}" \
		LDFLAGS="%{LDFLAGS}"

	make_install_targets += \
		NO_ARLIB=1 \
		ETCDIR=%{sysconfdir} \
		LIBDIR=%{libdir} \
		USRLIBDIR=%{libdir} \
		BINDIR=%{bindir} \
		SBINDIR=%{sbindir} \
		MANDIR=%{mandir} \
		INCLUDEDIR=%{includedir} \
		SHAREDIR=%{datadir}/%{name}

	install_cmds
		# Fix broken symlink.
		ln -svf libkeyutils.so.1 %{BUILDROOT}%{libdir}/libkeyutils.so
	end
end

packages
	package %{name}

	package libkeyutils
		template LIBS
	end

	package libkeyutils-devel
		template DEVEL

		requires += libkeyutils = %{thisver}
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
