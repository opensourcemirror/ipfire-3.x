###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = ca-certificates
version    = 2023.09
release    = 1

groups     = System/Base
url        = https://www.mozilla.org/
license    = Public Domain
summary    = The Mozilla CA root certificate bundle.

description
	This package contains the set of CA certificates chosen by the
	Mozilla Foundation for use with the Internet PKI.
end

# This package has no tarball.
sources    =

build
	arches = noarch

	requires
		openssl
		p11-kit >= 0.25
		python3
	end

	DIR_APP = %{DIR_SOURCE}

	build
		# Create file layout
		mkdir -pv certs
		cp certdata.txt blacklist.txt certs

		pushd certs
		python3 %{DIR_SOURCE}/certdata2pem.py
		popd

		(cat <<EOF
		# This is a bundle of X.509 certificates of public Certificate
		# Authorities.  It was generated from the Mozilla root CA list.
		# 
		# Source: mozilla/security/nss/lib/ckfw/builtins/certdata.txt
		#
		EOF
		) > ca-bundle.crt

		(cat <<EOF
		# This is a bundle of X.509 certificates of public Certificate
		# Authorities.  It was generated from the Mozilla root CA list.
		# These certificates are in the OpenSSL "TRUSTED CERTIFICATE"
		# format and have trust bits set accordingly.
		#
		# Source: mozilla/security/nss/lib/ckfw/builtins/certdata.txt
		#
		EOF
		) > ca-bundle.trust.crt

		mkdir -pv /etc/pki/ca-trust/source

		# Collect all certs for p11-kit
		for p in certs/*.tmp-p11-kit; do
			cat "${p}" >> /etc/pki/ca-trust/source/ca-bundle.trust.p11-kit
		done

		trust extract \
			--overwrite \
			--comment \
			--filter=certificates \
			--format=openssl-bundle \
			ca-bundle.trust
		cat ca-bundle.trust >> ca-bundle.trust.crt

		trust extract \
			--overwrite \
			--comment \
			--filter=ca-anchors \
			--format=pem-bundle \
			--purpose=server-auth \
			ca-bundle
		cat ca-bundle >> ca-bundle.crt
	end

	install
		# Create folder layout.
		mkdir -p %{BUILDROOT}/etc/pki/tls/certs/

		# Install files.
		install -p -m 644 ca-bundle.crt %{BUILDROOT}%{sysconfdir}/pki/tls/certs/ca-bundle.crt
		install -p -m 644 ca-bundle.trust.crt %{BUILDROOT}%{sysconfdir}/pki/tls/certs/ca-bundle.trust.crt

		ln -s certs/ca-bundle.crt %{BUILDROOT}%{sysconfdir}/pki/tls/cert.pem

		touch -r certdata.txt %{BUILDROOT}%{sysconfdir}/pki/tls/certs/ca-bundle.crt
		touch -r certdata.txt %{BUILDROOT}%{sysconfdir}/pki/tls/certs/ca-bundle.trust.crt

		# /etc/ssl/certs symlink for 3rd-party tools
		mkdir -pv -m 755 %{BUILDROOT}%{sysconfdir}/ssl
		ln -s ../pki/tls/certs %{BUILDROOT}%{sysconfdir}/ssl/certs
	end
end

packages
	package %{name}
end
