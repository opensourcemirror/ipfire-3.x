###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = ppp
version    = 2.5.0
release    = 1

groups     = System/Daemons
url        = https://ppp.samba.org/
license    = BSD and LGPLv2+ and GPLv2+ and Public Domain
summary    = The PPP (Point-to-Point Protocol) daemon

description
	The ppp package contains the PPP (Point-to-Point Protocol) daemon and
	documentation for PPP support. The PPP protocol provides a method for
	transmitting datagrams over serial point-to-point links. PPP is
	usually used to dial in to an ISP or other organization over a modem
	and phone line.
end

source_dl  = https://download.samba.org/pub/ppp/

build
	requires
		autoconf
		automake
		libpcap-devel
		libudev-devel
		libxcrypt-devel
		openssl-devel
		pam-devel
		systemd-devel
	end

	configure_options += --with-logfile-dir=/var/log

	install
		make install INSTROOT=%{BUILDROOT}

		touch /var/log/connect-errors
		mkdir -pv %{BUILDROOT}/etc/ppp

		# Reminder note
		# code used to be here to copy across IPFire2.x dialler etc scripts
		# something to replace those for IPFire3.x is likely nedeed somewhere

		touch %{BUILDROOT}/etc/ppp/secrets
		chmod -v 600 %{BUILDROOT}/etc/ppp/secrets
		ln -svf %{BUILDROOT}/etc/ppp/secrets /etc/ppp/pap-secrets
		ln -svf %{BUILDROOT}/etc/ppp/secrets /etc/ppp/chap-secrets
	end
end

packages
	package %{name}

		script postin
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postup
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postun
			systemctl daemon-reload >/dev/null 2>&1 || :
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
