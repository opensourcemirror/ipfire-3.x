###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = python3
major_ver  = 3.11
version    = %{major_ver}.5
release    = 1
thisapp    = Python-%{version}

groups     = Development/Languages
url        = https://www.python.org
license    = Python
summary    = Version 3 of the Python programming language.

description
	Python is an interpreted, interactive, object-oriented programming
	language often compared to Tcl, Perl, Scheme or Java. Python includes
	modules, classes, exceptions, very high level dynamic data types and
	dynamic typing. Python supports interfaces to many system calls and
	libraries, as well as to various windowing systems.
end

source_dl  = https://python.org/ftp/python/%{version}/
sources    = %{thisapp}.tar.xz

pylibdir   = %{libdir}/python%{major_ver}
dynload_dir= %{pylibdir}/lib-dynload

build
	requires
		autoconf
		automake
		bzip2
		bzip2-devel
		expat-devel >= 2.1.0
		findutils
		gdbm-devel >= 1.10
		gcc-c++
		glibc-devel >= 2.26
		gmp-devel
		libdb-devel
		libffi-devel
		ncurses-devel
		openssl-devel
		pkg-config
		readline-devel
		sqlite-devel
		tar
		tcl-devel
		util-linux
		xz-devel
		zlib-devel
	end

	export OPT       = %{CFLAGS}
	export LINKCC    = gcc

	prepare_cmds
		# Remove embedded copies of expat and libffi
		rm -rf Modules/{expat,zlib}
		rm -rf Modules/_ctypes/{darwin,libffi,libffi_arm_wince,libffi_msvc,libffi_osx}

		# Make all source files owned by root.
		chown root.root -R .
	end

	configure_options += \
		--with-platlibdir=%{lib} \
		--enable-ipv6 \
		--enable-shared \
		--with-computed-gotos=yes \
		--with-dbmliborder=gdbm:ndbm:bdb \
		--with-system-expat \
		--with-system-ffi \
		--with-system-libmpdec \
		--with-lto \
		--with-ssl-default-suites=openssl \
		--enable-loadable-sqlite-extensions \
		--without-ensurepip

	#test
	#	make test
	#end

	install_cmds
		install -d -m 0755 %{BUILDROOT}%{pylibdir}/site-packages/__pycache__
		install -d -m 0755 %{BUILDROOT}/usr/lib/python${major_ver}/site-packages/__pycache__

		# Switch all shebangs to refer to the specific Python version.
		LD_LIBRARY_PATH=. ./python Tools/scripts/pathfix.py \
			-i "%{bindir}/python%{major_ver}" \
			%{BUILDROOT}

		# Remove shebang lines from .py files that aren't executable, and
		# remove executability from .py files that don't have a shebang line:
		find %{BUILDROOT} -name \*.py \
			\( \( \! -perm /u+x,g+x,o+x -exec sed -e '/^#!/Q 0' -e 'Q 1' {} \; \
			-print -exec sed -i '1d' {} \; \) -o \( \
			-perm /u+x,g+x,o+x ! -exec grep -m 1 -q '^#!' {} \; \
			-exec chmod a-x {} \; \) \)

		# Get rid of backup files:
		find %{BUILDROOT}/ -name "*~" -exec rm -f {} \;
		rm -f %{BUILDROOT}%{pylibdir}/LICENSE.txt
	end
end

packages
	package %{name}
		# Define python-abi manually.
		provides
			python-abi = %{major_ver}
		end

		requires
			%{name}-libs = %{thisver}
		end
	end

	package %{name}-libs
		template LIBS

		requires
			expat >= 2.1.0
		end
	end

	package %{name}-devel
		template DEVEL

		requires
			%{name} = %{thisver}
			%{name}-libs = %{thisver}
		end

		files += \
			!%{libdir}/python*/config*/Makefile \
			!%{includedir}/python*/pyconfig.h \
			!%{pylibdir}/lib-dynload
	end

	package %{name}-test
		requires
			%{name} = %{thisver}
		end

		files
			%{pylibdir}/ctypes/test
			%{pylibdir}/distutils/testi
			%{pylibdir}/lib2to3/test
			%{pylibdir}/sqlite3/test
			%{pylibdir}/test
			%{pylibdir}/tkinter/test
			%{pylibdir}/unittest/test
			%{dynload_dir}/_ctypes_test*
			%{dynload_dir}/_testbuffer*
			%{dynload_dir}/_testcapi*
		end
	end

	package %{name}-tools
		requires
			%{name} = %{thisver}
		end

		files
			%{bindir}/python3-2to3
			%{bindir}/2to3*
			%{bindir}/idle*
			%{pylibdir}/Tools
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
